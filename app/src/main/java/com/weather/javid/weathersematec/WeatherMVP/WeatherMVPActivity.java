package com.weather.javid.weathersematec.WeatherMVP;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.weather.javid.weathersematec.Models.WeatherYahoo;
import com.weather.javid.weathersematec.MyWidgets.MyEditText;
import com.weather.javid.weathersematec.MyWidgets.MyTextView;
import com.weather.javid.weathersematec.R;

public class WeatherMVPActivity extends AppCompatActivity implements ContractClass.View {
    MyEditText inputCity;
    MyTextView cityName;
    Presenter presenter = new Presenter();
    ProgressDialog dialog;
    Context mContext = WeatherMVPActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_mvp);
        presenter.attachView(this);
        onBindViews();
        dialog = new ProgressDialog(mContext);
        dialog.setTitle("loading");
        dialog.setMessage("pleaseWait...");
    }

    private void onBindViews() {
        inputCity = findViewById(R.id.inputCity);
        cityName = findViewById(R.id.cityName);

        findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(false);
                presenter.validate(inputCity.text());
            }
        });
    }

    @Override
    public void successSearch(WeatherYahoo yahoo) {
        showLoading(false);

    }

    @Override
    public void onFail(String msg) {
        showLoading(false);
        cityName.setText("nabod");
    }

    @Override
    public void onNullInput() {
        cityName.setText("khali nade");
    }

    @Override
    public void showLoading(Boolean show) {
        if (show)
            dialog.show();
        else
            dialog.dismiss();
    }
}
