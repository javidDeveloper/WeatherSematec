package com.weather.javid.weathersematec.WeatherMVP;


import com.weather.javid.weathersematec.Models.WeatherYahoo;

public class Model implements ContractClass.Model {
    private ContractClass.Presenter presenter;
    RestRipository rest =new RestRipository();
    DataBaseRipository database= new DataBaseRipository();
    String word;
    @Override
    public void attachPresenter(ContractClass.Presenter presenter) {
        this.presenter = presenter;
        rest.attachModel(this);
        database.attachModel(this);
    }

    @Override
    public void search(String word) {
        this.word = word;
       rest.getData(this.word);
    }

    @Override
    public void onReciveData(WeatherYahoo yahoo, RepoType type) {
        if(type  == RepoType.Rest){
            database.saveDataToDatabase(yahoo);
        }else  if(type  == RepoType.Database){
            presenter.successSearch(yahoo);
        }
    }

    @Override
    public void onFailData(String e, RepoType type) {
        if(type  == RepoType.Rest){
          presenter.onFail("nout found rest");
        }else  if(type  == RepoType.Database){
           rest.getData(word);
        }
    }

}
