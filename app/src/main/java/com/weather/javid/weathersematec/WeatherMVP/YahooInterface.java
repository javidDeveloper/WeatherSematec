package com.weather.javid.weathersematec.WeatherMVP;

import com.weather.javid.weathersematec.Models.WeatherYahoo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YahooInterface {
    //https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys

    @GET("/yql")
    Call<WeatherYahoo> getDataFromYahoo(@Query("q")String query,
                                        @Query("format")String format);
}
