package com.weather.javid.weathersematec.WeatherMVP;

import com.orm.SugarRecord;

public class PojoDataBase extends SugarRecord<PojoDataBase> {
    private String city;
    private String temp;
    private String count;

    public PojoDataBase() {
    }

    public PojoDataBase(String city, String temp, String count) {
        this.city = city;
        this.temp = temp;
        this.count = count;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
