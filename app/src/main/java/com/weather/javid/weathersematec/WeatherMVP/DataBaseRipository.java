package com.weather.javid.weathersematec.WeatherMVP;

import com.weather.javid.weathersematec.Models.Query;
import com.weather.javid.weathersematec.Models.WeatherYahoo;

import java.util.List;

public class DataBaseRipository implements ModelRipository.Database {
    private Model model;

    @Override
    public void attachModel(Model model) {

        this.model = model;
    }

    @Override
    public void getData(String word) {
        List<PojoDataBase> city = PojoDataBase.find(PojoDataBase.class, "city like ? ", word);
        if (city != null) {
            model.onReciveData((WeatherYahoo) city,RepoType.Database);
        }else
            model.onFailData("dar db nabod",RepoType.Database);
    }

    @Override
    public void saveDataToDatabase(WeatherYahoo yahoo) {
        PojoDataBase dataBase = new PojoDataBase();
        dataBase.setCity(yahoo.getQuery().getResults().getChannel().getLocation().getCity()+"");
        dataBase.setCount(yahoo.getQuery().getCount().toString());
        dataBase.setTemp(yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        dataBase.save();

    }
}
