package com.weather.javid.weathersematec.WeatherMVP;

import android.view.View;

import com.weather.javid.weathersematec.Models.WeatherYahoo;

public class Presenter implements ContractClass.Presenter {
    Model model;
    private ContractClass.View view;


    public Presenter() {
        this.model = new Model();
    }


    @Override
    public void attachView(ContractClass.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void search(String word) {
        view.showLoading(true);
        model.search(word);
    }

    @Override
    public void validate(String word) {
        if (word != null) {
            search(word);
        } else {
            view.onNullInput();
        }
    }

    @Override
    public void successSearch(WeatherYahoo yahoo) {
        view.showLoading(false);
        view.successSearch(yahoo);
    }

    @Override
    public void onFail(String msg) {
        view.showLoading(false);
        view.onFail(msg);
    }
}
