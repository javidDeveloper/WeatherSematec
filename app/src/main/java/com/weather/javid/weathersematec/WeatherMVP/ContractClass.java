package com.weather.javid.weathersematec.WeatherMVP;

import com.weather.javid.weathersematec.Models.WeatherYahoo;

public interface ContractClass {
    interface View {
        void successSearch(WeatherYahoo yahoo);
        void onFail(String msg);
        void onNullInput();
        void showLoading(Boolean show);
    }

    interface Presenter {
        void attachView(View view);
        void search(String word);
        void validate(String word);
        void successSearch(WeatherYahoo yahoo);
        void onFail(String msg);
    }

    interface Model {
        void attachPresenter(Presenter  presenter);
        void search(String word);
        void onReciveData(WeatherYahoo yahoo, RepoType type);
        void onFailData(String e, RepoType type);

    }
}
