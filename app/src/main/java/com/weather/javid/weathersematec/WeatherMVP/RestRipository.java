package com.weather.javid.weathersematec.WeatherMVP;

import com.weather.javid.weathersematec.Models.WeatherYahoo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestRipository implements ModelRipository.Rest {
    private Model model;

    @Override
    public void attachModel(Model model) {

        this.model = model;
    }

    @Override
    public void getData(String word) {
        API.getClient().create(YahooInterface.class).getDataFromYahoo("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"\"word\", ir\")"
                , "json").enqueue(new Callback<WeatherYahoo>() {
            @Override
            public void onResponse(Call<WeatherYahoo> call, Response<WeatherYahoo> response) {
                model.onReciveData(response.body(), RepoType.Rest);
            }

            @Override
            public void onFailure(Call<WeatherYahoo> call, Throwable t) {
                model.onFailData(t.toString(), RepoType.Rest);
            }
        });
    }
}
