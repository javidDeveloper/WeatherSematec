package com.weather.javid.weathersematec.WeatherMVP;

import com.weather.javid.weathersematec.Models.WeatherYahoo;

public interface ModelRipository {

    interface Rest{
        void attachModel(Model model);
        void  getData(String word);
    }
    interface Database{
        void attachModel(Model model);
        void getData(String word);
        void saveDataToDatabase(WeatherYahoo yahoo);
    }
}
