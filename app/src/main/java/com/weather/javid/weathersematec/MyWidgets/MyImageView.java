package com.weather.javid.weathersematec.MyWidgets;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.squareup.picasso.Picasso;


public class MyImageView extends AppCompatImageView {
    Context mContext;
    public MyImageView(Context context) {
        super(context);
        this.mContext = context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;

    }
    public void url(int url){
        Picasso.with(mContext).load(url).into(this);
    }
}
